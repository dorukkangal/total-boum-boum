package org.totalboumboum.game.tournament.turning;

/*
 * Total Boum Boum
 * Copyright 2008-2012 Vincent Labatut 
 * 
 * This file is part of Total Boum Boum.
 * 
 * Total Boum Boum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Total Boum Boum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Total Boum Boum.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.util.Set;

import org.totalboumboum.game.match.Match;
import org.totalboumboum.game.rank.Ranks;
import org.totalboumboum.game.tournament.AbstractTournament;

/**
 * 
 * @author Vincent Labatut
 *
 */
public class TurningTournament extends AbstractTournament
{	private static final long serialVersionUID = 1L;
	
	public TurningTournament()
	{	
	}

	@Override
	public Ranks getOrderedPlayers() {
		// 
		return null;
	}

	@Override
	public void init() {
		// 
		
	}

	@Override
	public void progress() {
		// 
		
	}

	@Override
	public void finish() {
		// 
		
	}

	@Override
	public Match getCurrentMatch() {
		// 
		return null;
	}

	@Override
	public void matchOver() {
		// 
		
	}

	@Override
	public void roundOver() {
		// 
		
	}

	@Override
	public Set<Integer> getAllowedPlayerNumbers() {
		// 
		return null;
	}

}
