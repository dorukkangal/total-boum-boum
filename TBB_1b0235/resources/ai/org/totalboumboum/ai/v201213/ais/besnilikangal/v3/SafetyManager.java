package org.totalboumboum.ai.v201213.ais.besnilikangal.v3;

/*
 * Total Boum Boum
 * Copyright 2008-2012 Vincent Labatut 
 * 
 * This file is part of Total Boum Boum.
 * 
 * Total Boum Boum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Total Boum Boum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Total Boum Boum.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.totalboumboum.ai.v201213.adapter.communication.StopRequestException;
import org.totalboumboum.ai.v201213.adapter.data.AiBlock;
import org.totalboumboum.ai.v201213.adapter.data.AiBomb;
import org.totalboumboum.ai.v201213.adapter.data.AiFire;
import org.totalboumboum.ai.v201213.adapter.data.AiHero;
import org.totalboumboum.ai.v201213.adapter.data.AiStateName;
import org.totalboumboum.ai.v201213.adapter.data.AiTile;
import org.totalboumboum.ai.v201213.adapter.data.AiZone;

/**
 * TODO
 * 
 * @author Mustafa Besnili
 * @author Doruk Kangal
 */
public class SafetyManager
{
	/** IA associée à ce gestionnaire de sûreté */
	private BesniliKangal ai;
	/** Notre hero */
	private AiHero ownHero;
	/** zone de jeu */
	private AiZone zone;
	/** valeur pour une case complètement sûre (temps restant avant explosion : maximal) */
	public final static Double SAFE = Double.MAX_VALUE;
	/** valeur pour une case pas du tout sûre (temps restant avant explosion : aucun) */
	public final static Double FIRE = 0d;
	/** TODO */
	private LinkedHashMap<AiTile, Double> dangerousTiles;
	/** liste des bombes traitées au cours de cette itération (pour ne pas les traiter plusieurs fois) */
	private List<AiBomb> processedBombs;
	/** Les cases que notre IA peut atteidre et qui sont surs (pas de blast ou de flamme) */
	private LinkedHashMap<AiTile, Double> accessibleSafeTiles;

	/**
	 * @param ai
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public SafetyManager( BesniliKangal ai ) throws StopRequestException
	{
		ai.checkInterruption();
		this.ai = ai;
		this.ownHero = ai.ownHero;
		zone = ai.getZone();
		processedBombs = new ArrayList<AiBomb>();
		dangerousTiles = new LinkedHashMap<AiTile, Double>();
		accessibleSafeTiles = new LinkedHashMap<AiTile, Double>();
	}

	/**
	 * met à jour la matrice de sûreté
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public void update() throws StopRequestException
	{
		ai.checkInterruption();
		updateDangerousTiles();
		updateAccessibleSafeTiles();
	}

	/**
	 * mise à jour de la matrice de sûreté
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	private void updateDangerousTiles() throws StopRequestException
	{
		ai.checkInterruption();
		dangerousTiles.clear();
		processedBombs.clear();

		if ( !ai.ownHero.hasThroughFires() )
		{
			for ( AiTile tile : zone.getTiles()  )
			{
				ai.checkInterruption();
				List<AiFire> fires = tile.getFires();
				List<AiBomb> bombs = tile.getBombs();
				List<AiBlock> blocks = tile.getBlocks();

				if ( !fires.isEmpty() )
					dangerousTiles.put( tile, FIRE );
				else if ( !blocks.isEmpty() && blocks.get( 0 ).getState().getName() == AiStateName.BURNING )
					dangerousTiles.put( tile, FIRE );
				else if ( bombs.size() > 0 )
				{
					AiBomb bomb = bombs.iterator().next();
					processBomb( bomb );
				}
			}
		}
	}

	/**
	 * traite la bombe passée en paramètre
	 * 
	 * @param bomb
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	private void processBomb( AiBomb bomb ) throws StopRequestException
	{
		ai.checkInterruption();
		if ( !processedBombs.contains( bomb ) )
		{
			List<AiTile> blastTiles = new ArrayList<AiTile>();
			List<AiBomb> blastBombs = new ArrayList<AiBomb>();
			Double minExplosionTime = getAllBlast( bomb, blastTiles, blastBombs, SAFE );
			processedBombs.addAll( blastBombs );
			for ( AiTile blast : blastTiles )
			{
				ai.checkInterruption();
				if ( !dangerousTiles.containsKey( blast ) || dangerousTiles.get( blast ) > minExplosionTime )
					dangerousTiles.put( blast, minExplosionTime );
			}
		}
	}

	/**
	 * TODO
	 * 
	 * @param bomb
	 * @param blastTiles
	 * @param blastBombs
	 * @param minExplosionTime 
	 * 
	 * @return ?
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public double getAllBlast( AiBomb bomb, List<AiTile> blastTiles, List<AiBomb> blastBombs, Double minExplosionTime ) throws StopRequestException
	{
		ai.checkInterruption();
		if ( !blastBombs.contains( bomb ) )
		{
			blastBombs.add( bomb );
			List<AiTile> tempBlast = bomb.getBlast();
			blastTiles.addAll( tempBlast );
			long explosionTime = bomb.getNormalDuration() - bomb.getElapsedTime();
			if ( minExplosionTime > explosionTime )
				minExplosionTime = (double) explosionTime;
			for ( AiTile tile : tempBlast )
			{
				ai.checkInterruption();
				Collection<AiBomb> bList = tile.getBombs();
				if ( bList.size() > 0 )
				{
					AiBomb b = bList.iterator().next();
					getAllBlast( b, blastTiles, blastBombs, minExplosionTime );
				}
			}
		}
		return minExplosionTime;
	}

	/**
	 * Mettre a jour les dont notre IA peut attéindre
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	private void updateAccessibleSafeTiles() throws StopRequestException
	{
		ai.checkInterruption();
		accessibleSafeTiles.clear();
		LinkedHashMap<AiTile, Double> aaccessibleTilesWithArrivalTime = ai.tileOperation.getAccessibleTiles();
		for ( Entry<AiTile, Double> entry : aaccessibleTilesWithArrivalTime.entrySet() )
		{
			ai.checkInterruption();
			AiTile accessibleTile = entry.getKey();
			if ( isSafe( accessibleTile ) && !ai.itemOperation.getMalusTiles().contains( accessibleTile ) )
			{
				Double arrivalTime = entry.getValue();
				accessibleSafeTiles.put( accessibleTile, arrivalTime );
			}
		}
	}

	/////////////////////////////////////////////////////////////////
	// GETTERS			/////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/**
	 * To get the accessible dangerous tiles from a given source tile.
	 * 
	 * @return List of all tiles that accessible from a given source tile.
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public LinkedHashMap<AiTile, Double> getDangerousTiles() throws StopRequestException
	{
		ai.checkInterruption();
		return dangerousTiles;
	}

	/**
	 * To get the accessible safe tiles from a given source tile.
	 * 
	 * @return List of all tiles that accessible from a given source tile.
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public LinkedHashMap<AiTile, Double> getAccessibleSafeTiles() throws StopRequestException
	{
		ai.checkInterruption();
		return accessibleSafeTiles;
	}

	/**
	 * Returns all accessible tiles that within given radius. Uses
	 * "Manhattan Distance" to calculate distances, A* is too slow for this. <br/>
	 * 
	 * @param timeLimit
	 *            Max arrival time
	 * 
	 * @return List of the tiles within the circle with the center as the given
	 *         tile and the given radius .
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public Set<AiTile> getAccessibleSafeTilesWithinArrivalTime( Double timeLimit ) throws StopRequestException
	{
		ai.checkInterruption();
		Set<AiTile> tilesWithinArrivalTime = new LinkedHashSet<AiTile>();
		for ( Map.Entry<AiTile, Double> accessibleTileWithArrivalTime : accessibleSafeTiles.entrySet() )
		{
			ai.checkInterruption();
			if ( accessibleTileWithArrivalTime.getValue() <= timeLimit )
				tilesWithinArrivalTime.add( accessibleTileWithArrivalTime.getKey() );
		}
		return tilesWithinArrivalTime;
	}

	/**
	 * Finds the closest accessible safe tile to this AI's own hero's tile. Uses
	 * "Manhattan Distance" to calculate the distances. <br />
	 * 
	 * @return Closest safe tile to this AI's hero.
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public AiTile getClosestSafeTile() throws StopRequestException
	{
		ai.checkInterruption();
		if ( !accessibleSafeTiles.isEmpty() )
		{
			AiTile result = null;
			Double timeLimit = 5 * ai.tileOperation.passTimeByTile();
			for ( Entry<AiTile, Double> entry : accessibleSafeTiles.entrySet() )
			{
				ai.checkInterruption();
				AiTile accessibleTile = entry.getKey();
				Double arrivalTime = entry.getValue();
				if ( !accessibleTile.getItems().isEmpty() && ai.itemOperation.isGoodItem( accessibleTile.getItems().get( 0 ) ) && timeLimit >= arrivalTime )
				{
					timeLimit = arrivalTime;
					result = accessibleTile;
				}
			}
			if ( result != null )
				return result;

			result = ai.tileOperation.getBiggestTile();
			if ( result != null )
				return result;

			return accessibleSafeTiles.keySet().iterator().next();
		}
		return ownHero.getTile();
	}

	/**
	 * renvoie le niveau de sécurité de la case passée en paramètre (i.e. le
	 * temps restant avant explosion)
	 * 
	 * @param tile
	 *            Case à traiter.
	 * 
	 * @return Niveau de sécurité.
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public Double getSafetyLevel( AiTile tile ) throws StopRequestException
	{
		ai.checkInterruption();
		Double level = dangerousTiles.get( tile );
		return ( level == null ) ? SAFE : level;
	}

	/**
	 * détermine si le niveau de sécurité de la case passée en paramètre est
	 * maximal (ce traitement n'est pas très subtil : en cas d'explosion
	 * potentielle, on pourrait calculer le temps nécessaire pour atteindre la
	 * case et déterminer si c'est possible de passer dessus avant l'explosion)
	 * 
	 * @param tile
	 *            Case à traiter.
	 * 
	 * @return {@code true} ssi la case est sûre.
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public boolean isSafe( AiTile tile ) throws StopRequestException
	{
		ai.checkInterruption();
		Double level = getSafetyLevel( tile );
		return ( level == SAFE );
	}
}
