package org.totalboumboum.ai.v201213.ais.besnilikangal.v3.criterion;

import org.totalboumboum.ai.v201213.adapter.agent.AiUtilityCriterionInteger;
import org.totalboumboum.ai.v201213.adapter.communication.StopRequestException;
import org.totalboumboum.ai.v201213.adapter.data.AiTile;
import org.totalboumboum.ai.v201213.ais.besnilikangal.v3.BesniliKangal;

/**
 * Cette critere a été utilisé pour évaluer le temps d'arrivée pour une case. Le
 * temps est proportionnel à la distance entre notre IA et cette case.
 * 
 * @author Doruk Kangal
 * @author Mustafa Besnili
 */
public class Duree extends AiUtilityCriterionInteger<BesniliKangal>
{
	/** Nom de ce critère */
	public static final String NAME = "Duree";

	/**
	 * @param ai
	 *            l'agent concerné.
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public Duree( BesniliKangal ai ) throws StopRequestException
	{
		super( ai, NAME, MIN, MAX );
		ai.checkInterruption();
		this.ai = ai;
	}

	/**
	 * @param ai
	 *            l'agent concerné.
	 * @param min
	 * @param max
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public Duree( BesniliKangal ai, int min, int max ) throws StopRequestException
	{
		super( ai, NAME, min, max );
		ai.checkInterruption();
		MIN = min;
		MAX = max;
		ai.checkInterruption();
		this.ai = ai;
	}

	/**La valeur maximale pour cette critere */
	private static int MAX = 3;
	/**La valeur minimale pour cette critere*/
	private static int MIN = 0;
	/** La valeur multipliant puisque le critere renvoye la 0 pour 0-2 et 1 pour 3-5 etc*/
	private final int MULTIPLIER = 3;

	/////////////////////////////////////////////////////////////////
	// PROCESS 					/////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	@Override
	public Integer processValue( AiTile tile ) throws StopRequestException
	{
		ai.checkInterruption();
		int result = MAX;
		Double arrivalTime = ai.safetyManager.getAccessibleSafeTiles().get( tile );
		if ( arrivalTime != null )
		{
			double tileDistance = arrivalTime / ai.tileOperation.passTimeByTile();
			int max = MAX;
			while ( MIN < max )
			{
				ai.checkInterruption();
				if ( max * MULTIPLIER < tileDistance )
					return max;
				max--;
			}
		}
		return result;
	}
}
