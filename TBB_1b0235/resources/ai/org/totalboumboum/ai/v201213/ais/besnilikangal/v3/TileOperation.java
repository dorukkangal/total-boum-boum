package org.totalboumboum.ai.v201213.ais.besnilikangal.v3;

import java.util.AbstractMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.totalboumboum.ai.v201213.adapter.communication.StopRequestException;
import org.totalboumboum.ai.v201213.adapter.data.AiHero;
import org.totalboumboum.ai.v201213.adapter.data.AiTile;
import org.totalboumboum.ai.v201213.adapter.data.AiZone;
import org.totalboumboum.engine.content.feature.Direction;

/**
 * La class qui contient des methods utiles pour les operations des cases.
 * 
 * @author Doruk Kangal
 * @author Mustafa Besnili
 */
public class TileOperation
{
	/** Notre IA */
	private BesniliKangal ai;
	/** Notre hero */
	private AiHero ownHero;
	/** La zone de jeu */
	private AiZone zone;
	/** Les cases que notre IA peut atteindre */
	private LinkedHashMap<AiTile, Double> accessibleTilesWithArrivalTime;
	/** La case dont la valeur d'utilité est maximale */
	private AiTile biggestTile;

	/**
	 * @param ai
	 *            l'agent concerné.
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public TileOperation(BesniliKangal ai) throws StopRequestException
	{
		ai.checkInterruption();
		this.ai = ai;
		this.ownHero = ai.ownHero;
		zone = ai.getZone();
		accessibleTilesWithArrivalTime = new LinkedHashMap<AiTile, Double>();
	}

	/////////////////////////////////////////////////////////////////
	// UPDATES			/////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/**
	 * Mettre a jour les cases
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public void update() throws StopRequestException
	{
		ai.checkInterruption();
		updateAccessibleTiles();
		updateBiggestTile();
	}

	/**
	 * Method recursive qui trouve les cases que notre IA peut atteindre.
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	private void updateAccessibleTiles() throws StopRequestException
	{
		ai.checkInterruption();
		accessibleTilesWithArrivalTime.clear();
		Double passTimeByTile = passTimeByTile();
		LinkedList<Entry<AiTile, Double>> tileTimePairs = new LinkedList<Entry<AiTile, Double>>();
		tileTimePairs.add( new AbstractMap.SimpleEntry<AiTile, Double>( ownHero.getTile(), 0d ) );
		int index = 0;
		while ( index < tileTimePairs.size() )
		{
			ai.checkInterruption();
			AiTile sourceTile = tileTimePairs.get( index ).getKey();
			Double arrivalTime = tileTimePairs.get( index ).getValue();
			for ( AiTile neighbor : sourceTile.getNeighbors() )
			{
				ai.checkInterruption();
				if ( neighbor.isCrossableBy( ownHero ) && !ai.itemOperation.getMalusTiles().contains( neighbor ) && !hasAddedBefore( tileTimePairs, neighbor ) )
					tileTimePairs.add( new AbstractMap.SimpleEntry<AiTile, Double>( neighbor, arrivalTime + passTimeByTile ) );
			}
			index++;
		}
		for ( Entry<AiTile, Double> entry : tileTimePairs )
		{
			ai.checkInterruption();
			accessibleTilesWithArrivalTime.put( entry.getKey(), entry.getValue() );
		}
	}

	/**
	 * @param tileTimePairs 
	 * @param givenTile
	 * 
	 * @return TODO
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	private boolean hasAddedBefore( LinkedList<Entry<AiTile, Double>> tileTimePairs, AiTile givenTile ) throws StopRequestException
	{
		ai.checkInterruption();
		for ( Entry<AiTile, Double> entry : tileTimePairs )
		{
			ai.checkInterruption();
			if ( givenTile == entry.getKey() )
				return true;
		}
		return false;
	}

	/**
	 * Mettre a jour la case dont la valeur d'utilité est maximal.
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	private void updateBiggestTile() throws StopRequestException
	{
		ai.checkInterruption();
		AiTile destinationTile = ownHero.getTile();

		Map<AiTile, Float> utilitiesByTile = ai.utilityHandler.getUtilitiesByTile();
		Float tileUtility = Float.MIN_VALUE;
		for ( Entry<AiTile, Float> utilityByTile : utilitiesByTile.entrySet() )
		{
			ai.checkInterruption();
			if ( tileUtility < utilityByTile.getValue() && ai.safetyManager.isSafe( utilityByTile.getKey() ) )
			{
				destinationTile = utilityByTile.getKey();
				tileUtility = utilityByTile.getValue();
			}
		}
		biggestTile = destinationTile;
	}

	/////////////////////////////////////////////////////////////////
	// GETTERS			/////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/**
	 * To get the accessible tiles from a given source tile.
	 * 
	 * @return List of all tiles that accessible from a given source tile.
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public LinkedHashMap<AiTile, Double> getAccessibleTiles() throws StopRequestException
	{
		ai.checkInterruption();
		return accessibleTilesWithArrivalTime;
	}

	/**
	 * @param givenTile
	 * 
	 * @return TODO
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public Double getArrivalTime( AiTile givenTile ) throws StopRequestException
	{
		ai.checkInterruption();
		Double result = accessibleTilesWithArrivalTime.get( givenTile );
		return ( result == null ) ? Double.MAX_VALUE : result;
	}

	/**
	 * Biggest Tile getter Renvoyer la case dont la valeur d'utilité est
	 * maximal.
	 * 
	 * @return AiTile
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public AiTile getBiggestTile() throws StopRequestException
	{
		ai.checkInterruption();
		return biggestTile;
	}

	/**
	 * Retourner les cases de l'ennemi en considerant notre flamme.
	 * 
	 * @param bombRange
	 * 
	 * @return Set<AiTile>
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public Set<AiTile> getEnnemyTilesInBombRange( int bombRange ) throws StopRequestException
	{
		ai.checkInterruption();
		Set<AiTile> tilesWithinRadius = new HashSet<AiTile>();
		for ( AiHero ennemy : zone.getRemainingOpponents() )
		{
			ai.checkInterruption();
			AiTile neighbor = ennemy.getTile();
			if ( !ownHero.getTile().equals( neighbor ) )
				tilesWithinRadius.add( neighbor );
			ai.checkInterruption();
			for ( Direction direction : Direction.getPrimaryValues() )
			{
				ai.checkInterruption();
				int i = 1;
				neighbor = ennemy.getTile();
				while ( i <= bombRange )
				{
					ai.checkInterruption();
					neighbor = neighbor.getNeighbor( direction );
					if ( !neighbor.isCrossableBy( ennemy ) || ai.safetyManager.isSafe( neighbor ) )
						break;
					tilesWithinRadius.add( neighbor );
					i++;
				}
			}
		}
		tilesWithinRadius.remove( ownHero.getTile() );
		return tilesWithinRadius;
	}

	/**
	 * Renvoyer la valeur en double qui correspond a la valeur que notre IA peux
	 * traverser une case.
	 * 
	 * @return double
	 * 
	 * @throws StopRequestException
	 *             Au cas où le moteur demande la terminaison de l'agent.
	 */
	public Double passTimeByTile() throws StopRequestException
	{
		ai.checkInterruption();
		return 1000 * ( ownHero.getTile().getSize() / ownHero.getWalkingSpeed() );
	}
}
